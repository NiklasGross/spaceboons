/**
 *
 * Klasse f�r ein Datum
 *
 * @version 1.0 vom 16.10.2016
 * @author Otto Werse
 */
public class Datum {

	// Anfang Attribute
	private int tag;
	private int monat;
	// Ende Attribute

	public Datum(int tag, int monat) {
		this.tag = tag;
		this.monat = monat;
	}

	public Datum() {
		this.tag = 0;
		this.monat = 0;
	}

	// Anfang Methoden
	public int getTag() {
		return tag;
	}

	public void setTag(int tag) {
		this.tag = tag;
	}

	public int getMonat() {
		return monat;
	}

	public void setMonat(int monat) {
		this.monat = monat;
	}

	// Ende Methoden
} // end of Datum

/**
 *
 * Testklasse f�r Julianische Osterformel & Erster Georgianischer
 * Weihnachtsfeiertag
 *
 * @version 1.0 vom 16.10.2016
 * @author Otto Werse & Sophie Schulz
 */
public class Test {

	public static void main(String[] args) {
		// Berechnung von katholischer Ostersonntag = orthodoxes Weihnachtsfest

		Datum o1 = new Datum(); // Erstellen von zwei Objekten der Klasse Datum,
								// je eins f�r Ostern und Weihnachten
		Datum w1 = new Datum();
		boolean match1 = false; // Boolean f�r das Abbrechen der Schleife
		int i1; // Laufzeitvariable f�r die Schleife initialisieren
		Jahr j1 = new Jahr(2000); // Neues Objekt der Klasse Jahr erstellen
		System.out.print(
				"Katholischer Ostersonntag und Orthodoxes Weihnachtsfest fallen zum ersten mal auf eine Tag im Jahre: ");
		for (i1 = 2000; i1 <= 1000000 && match1 == false; i1++) {
			// F�r die ersten 998000 Jahre nach 2000
			j1.setJahreszahl(i1); // Jahreszahl von j1 mit i1 belegen

			// Ostersonntag berechnen
			o1 = j1.getKOstern(); // Katholischen Ostersonntag berechnen lassen
			int o1Tag = o1.getTag(); // Tag und Monat des Datums auslesen
			int o1Monat = o1.getMonat();

			// 1. Weihnachtsfeiertag berechnen
			w1 = j1.getOWeihnachten(); // Orthodoxen 1. Weihnachtsfeiertag
										// berechnen lassen
			int w1Tag = w1.getTag(); // Tag und Monat des Datums auslesen
			int w1Monat = w1.getMonat();

			if (o1Tag == w1Tag && o1Monat == w1Monat) { // Wenn Tag und Monat
														// beider Daten gleich
														// sind
				match1 = true; // Schleife beenden
				// Jahreszahl ausgeben
				System.out.println(j1.getJahreszahl() + ".");
			} // end of if
		} // end of for

		if (j1.getJahreszahl() >= 2090) { // Wenn die Jahreszahl h�her ist als
											// Christians wahrscheinliche
											// Lebenserwartung
			System.out.println("Da muss Christian aber lange warten!");
		} // end of if

		System.out.println(""); // Leerzeile

		// Berechnung von orthodoxer Ostersonntag = katholisches Weihnachtsfest

		Datum o2 = new Datum(); // Erstellen von zwei Objekten der Klasse Datum,
								// je eins f�r Ostern und Weihnachten
		Datum w2 = new Datum();
		// Date test = new Date(16, 4);
		boolean match2 = false; // Boolean f�r das Abbrechen der Schleife
		int i2; // Laufzeitvariable f�r die Schleife initialisieren
		Jahr j2 = new Jahr(2000); // Neues Objekt der Klasse Jahr erstellen
		System.out.print(
				"Orthodoxer Ostersonntag und Katholisches Weihnachtsfest fallen zum ersten mal auf eine Tag im Jahre: ");
		for (i2 = 2000; i2 <= 1000000000 && match2 == false; i2++) {
			// F�r die ersten 998000 Jahre nach 2000
			j2.setJahreszahl(i2); // Jahreszahl von j2 mit i2 belegen

			// Ostersonntag berechnen
			o2 = j2.getOOstern(); // Orthodoxen Ostersonntag berechnen lassen
			int o2Tag = o2.getTag(); // Tag und Monat des Datums auslesen
			int o2Monat = o2.getMonat();

			// 1. Weihnachtsfeiertag berechnen
			w2 = j2.getKWeihnachten(); // Katholischen 1. Weihnachtsfeiertag
										// "berechnen" lassen
			int w2Tag = w2.getTag(); // Tag und Monat des Datums auslesen
			int w2Monat = w2.getMonat();

			if (o2Tag == w2Tag && o2Monat == w2Monat) { // Wenn Tag und Monat
														// beider Daten gleich
														// sind
				match2 = true; // Schleife beenden
				// Jahreszahl ausgeben
				System.out.println(j2.getJahreszahl() + ".");
			} // end of if
		} // end of for

		if (j2.getJahreszahl() >= 2090) { // Wenn die Jahreszahl h�her ist als
											// Christians wahrscheinliche
											// Lebenserwartung
			System.out.println("Da muss Christian aber lange warten!");
		} // end of if

	} // end of main

} // end of class Test
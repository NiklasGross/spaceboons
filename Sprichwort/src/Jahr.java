/**
 *
 * Klasse f�r ein Jahr inklusive Methoden zur berechnung von Julianischem
 * Ostersonntag, Gregorianischen Ostersonntag, Erstem Julianischem
 * Weihnachtsfeiertag & Erstem Gregorianischen Weihnachtsfeiertag
 *
 * @version 1.0 vom 16.10.2016
 * @author Otto Werse & Sophie Schulz
 */
public class Jahr {

	// Anfang Attribute
	private int jahreszahl;
	private Datum kWeihnachten = new Datum(25, 12);
	// Ende Attribute

	public Jahr(int jahreszahl) {
		this.jahreszahl = jahreszahl;
	}

	public Jahr() {
		this.jahreszahl = 0;
	}

	// Anfang Methoden

	public void setJahreszahl(int jahreszahl) {
		this.jahreszahl = jahreszahl;
	}

	public int getJahreszahl() {
		return jahreszahl;
	}

	public Datum getOOstern() { // Methode f�r das Berechnen des orthodoxen
								// Ostersonntags
		Datum oOstern = new Datum(); // Neues Objekt der Klasse Date
		int X = jahreszahl; // Anwendung der gau�schen Osterformel
		int M = 15;
		int S = 0;
		int A = X % 19;
		int D = (19 * A + M) % 30;
		int R = (D + A / 11) / 29;
		int OG = 21 + D - R;
		int Z = 7 - (X + X / 4 + S) % 7;
		int OE = 7 - (OG - Z) % 7;
		int OS = OG + OE;
		int day = OS + (X / 100) - (X / 400) - 2;

		int f = 28;
		boolean s = isSchaltjahr(jahreszahl); // L�nge des Februars berechnen

		if (s == true) { // Wenn das Jahr ein Schlatjahr ist
			f = 29; // Februar hat 29 Tage
		} // end of if
		else { // Sonst
			f = 28; // Februar hat 28 Tage
		} // end of if-else
			// In den Folgenden 74 Zeilen wird berechnet zu welchem Monat der
			// ausgerechnete Tag geh�rt.
		if (day > 31) {
			if (day > 31 + 30) {
				if (day > 31 + 30 + 31) {
					if (day > 31 + 30 + 31 + 30) {
						if (day > 31 + 30 + 31 + 30 + 31) {
							if (day > 31 + 30 + 31 + 30 + 31 + 31) {
								if (day > 31 + 30 + 31 + 30 + 31 + 31 + 30) {
									if (day > 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31) {
										if (day > 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30) {
											if (day > 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31) {
												if (day > 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31 + 31) {
													if (day >= 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31 + 31
															+ f) {
														oOstern.setTag(day - (31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30
																+ 31 + 31 + f));
														oOstern.setMonat(3);
													} // end of if
													else {
														oOstern.setTag(day - (31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30
																+ 31 + 31));
														oOstern.setMonat(2);
													} // end of if-else
												} // end of if
												else {
													oOstern.setTag(
															day - (31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31));
													oOstern.setMonat(1);
												} // end of if-else
											} // end of if
											else {
												oOstern.setTag(day - (31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30));
												oOstern.setMonat(12);
											} // end of if-else
										} // end of if
										else {
											oOstern.setTag(day - (31 + 30 + 31 + 30 + 31 + 31 + 30 + 31));
											oOstern.setMonat(11);
										} // end of if-else
									} // end of if
									else {
										oOstern.setTag(day - (31 + 30 + 31 + 30 + 31 + 31 + 30));
										oOstern.setMonat(10);
									} // end of if-else
								} // end of if
								else {
									oOstern.setTag(day - (31 + 30 + 31 + 30 + 31 + 31));
									oOstern.setMonat(9);
								} // end of if-else
							} // end of if
							else {
								oOstern.setTag(day - (31 + 30 + 31 + 30 + 31));
								oOstern.setMonat(8);
							} // end of if-else
						} // end of if
						else {
							oOstern.setTag(day - (31 + 30 + 31 + 30));
							oOstern.setMonat(7);
						} // end of if-else
					} // end of if
					else {
						oOstern.setTag(day - (31 + 30 + 31));
						oOstern.setMonat(6);
					} // end of if-else
				} // end of if
				else {
					oOstern.setTag(day - (31 + 30));
					oOstern.setMonat(5);
				} // end of if-else
			} // end of if
			else {
				oOstern.setTag(day - (31));
				oOstern.setMonat(4);
			} // end of if-else
		} // end of if
		else {
			oOstern.setTag(day);
			oOstern.setMonat(3);
		} // end of if-else
		return oOstern;
	}

	public Datum getKOstern() { // Methode zum Berechnen des Katholischen
								// Ostersonntags
		Datum kOstern = new Datum(); // Neues Objekt der Klasse Datum
		int X = jahreszahl; // Anwendung der Gau�schen Osterformel
		int K = X / 100;
		//System.out.println(K);
		int M = 15 + (3 * K + 3) / 4 - (8 * K + 13) / 25;
		//System.out.println(M);
		int S = 2 - (3 * K + 3) / 4;
		//System.out.println(S);
		int A = X % 19;
		//System.out.println(A);
		int D = (19 * A + M) % 30;
		//System.out.println(D);
		int R = (D + A / 11) / 29;
		//System.out.println(R);
		int OG = 21 + D - R;
		//System.out.println(OG);
		int Z = 7 - (X + X / 4 + S) % 7;
		//System.out.println(Z);
		int OE = 7 - (OG - Z) % 7;
		//System.out.println(OE); 
		int datum = OG + OE;
		if (datum <= 31) { // Wenn Datum kleiner gleich 31 ist
			kOstern.setTag(datum);
			kOstern.setMonat(3); // Monat ist M�rz
		} // end of if
		else { // Sonst
			kOstern.setTag(datum - 31);
			kOstern.setMonat(4); // Monat ist April
		} // end of if
		return kOstern;
	}

	public Datum getOWeihnachten() {
		Datum oWeihnachten = new Datum();
		int v = 0;
		int i = 2000;
		if (jahreszahl > 2000) { // Berechnung der Verschiebung vom 7. Januar
			for (i = 2000; i <= jahreszahl; i = i + 100) {
				if (i % 400 != 0) {
					v++;
				} // end of if
			} // end of for
			//System.out.println(v);
		} // end of if
		int day = 7 + v;
		int f = 28;
		boolean s = isSchaltjahr(jahreszahl);
		if (s == true) { // Wenn das Jahr ein Schaltjahr ist
			f = 29; // Februar hat 29 Tage
		} // end of if
		else { // Sonst
			f = 28; // Februar hat 28 Tage
		} // end of if-else
			// In den folgenden 74 Zeilen wird berechnet zu welchem Monat der
			// ausgerechnete Tag geh�rt.
		if (day > 31) {
			if (day > 31 + f) {
				if (day > 31 + f + 31) {
					if (day > 31 + f + 31 + 30) {
						if (day > 31 + f + 31 + 30 + 31) {
							if (day > 31 + f + 31 + 30 + 31 + 30) {
								if (day > 31 + f + 31 + 30 + 31 + 30 + 31) {
									if (day > 31 + f + 31 + 30 + 31 + 30 + 31 + 31) {
										if (day > 31 + f + 31 + 30 + 31 + 30 + 31 + 31 + 30) {
											if (day > 31 + f + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31) {
												if (day > 31 + f + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30) {
													if (day >= 31 + f + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30
															+ 31) {
														oWeihnachten.setTag(day - (31 + f + 31 + 30 + 31 + 30 + 31 + 31
																+ 30 + 31 + 30 + 31));
														oWeihnachten.setMonat(1);
													} // end of if
													else {
														oWeihnachten.setTag(day - (31 + f + 31 + 30 + 31 + 30 + 31 + 31
																+ 30 + 31 + 30));
														oWeihnachten.setMonat(12);
													} // end of if-else
												} // end of if
												else {
													oWeihnachten.setTag(
															day - (31 + f + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31));
													oWeihnachten.setMonat(11);
												} // end of if-else
											} // end of if
											else {
												oWeihnachten.setTag(day - (31 + f + 31 + 30 + 31 + 30 + 31 + 31 + 30));
												oWeihnachten.setMonat(10);
											} // end of if-else
										} // end of if
										else {
											oWeihnachten.setTag(day - (31 + f + 31 + 30 + 31 + 30 + 31 + 31));
											oWeihnachten.setMonat(9);
										} // end of if-else
									} // end of if
									else {
										oWeihnachten.setTag(day - (31 + f + 31 + 30 + 31 + 30 + 31));
										oWeihnachten.setMonat(8);
									} // end of if-else
								} // end of if
								else {
									oWeihnachten.setTag(day - (31 + f + 31 + 30 + 31 + 30));
									oWeihnachten.setMonat(7);
								} // end of if-else
							} // end of if
							else {
								oWeihnachten.setTag(day - (31 + f + 31 + 30 + 31));
								oWeihnachten.setMonat(6);
							} // end of if-else
						} // end of if
						else {
							oWeihnachten.setTag(day - (31 + f + 31 + 30));
							oWeihnachten.setMonat(5);
						} // end of if-else
					} // end of if
					else {
						oWeihnachten.setTag(day - (31 + f + 31));
						oWeihnachten.setMonat(4);
					} // end of if-else
				} // end of if
				else {
					oWeihnachten.setTag(day - (31 + f));
					oWeihnachten.setMonat(3);
				} // end of if-else
			} // end of if
			else {
				oWeihnachten.setTag(day - (31));
				oWeihnachten.setMonat(2);
			} // end of if-else
		} // end of if
		else {
			oWeihnachten.setTag(day);
			oWeihnachten.setMonat(1);
		} // end of if-else
		return oWeihnachten;
	}

	public Datum getKWeihnachten() { // Methode zur "Berechnung" des
										// Katholischen 1. Weihnachtsfeiertags
		return this.kWeihnachten;
	}

	public boolean isSchaltjahr(int jahr) { // Methode die Schaltjahre
											// identifiziert
		boolean schaltjahr = false;
		int jahrdurch4 = jahr % 4;
		int jahrdurch100 = jahr % 100;
		int jahrdurch400 = jahr % 400;
		if (jahr >= 1582) {
			if (jahrdurch4 == 0) {
				if (jahrdurch100 == 0) {
					if (jahrdurch400 == 0) {
						schaltjahr = true;
					} else {
						schaltjahr = false;
					}
				} else {
					schaltjahr = true;
				}
			} else {
				schaltjahr = false;
			}
		} else {
			if (jahrdurch4 == 0) {
				schaltjahr = true;
			} else {
				schaltjahr = false;
			}
		} // end of if-else
		return schaltjahr;
	}
	// Ende Methoden
} // end of Jahr
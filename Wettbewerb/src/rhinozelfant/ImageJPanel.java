package rhinozelfant;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ImageJPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Image img; //Bild erstellen
	
	public Image getImg() { //Bild getter
		return img;
	}

	public void setImg(String file) { //Bild setter
		try {
			img = ImageIO.read(new File(file));
		} catch (Exception e) {
		}

	}

	public ImageJPanel() {
		super();
		// Bild einbinden
		try {
			img = ImageIO.read(new File("C:/Users/Ian Miller/Desktop/rhinozelfant1.jpg"));
		} catch (Exception e) {
		}
	}
	
	public void rhinoFinden() {		//Methode die die Schuppen des Rhinozelfanten findet.
	
		BufferedImage bimg = (BufferedImage) img; //erstellen einer tempor�ren Kopie des Bildes
		for (int x = bimg.getMinX(); x < bimg.getWidth() - 1; x++) { 		//Hier wird das Bild mit zwei Schleifen
			for (int y = bimg.getMinY(); y < bimg.getHeight() - 1; y++) {	//von oben nach unten Pixel f�r Pixel durchgeschaut.
				//Da wir wissen, dass die Schuppen des Rhinozelfants 2*2 Pixel gro� und in der gleiche Farbe sind, schaut das Programm sich die 3 umliegenden Pixel an.
				Color c = new Color(bimg.getRGB((x), (y))); 		
				Color c1 = new Color(bimg.getRGB((x + 1), (y)));
				Color c2 = new Color(bimg.getRGB((x), (y + 1)));
				Color c3 = new Color(bimg.getRGB((x + 1), (y + 1)));
				Color white = new Color(255, 255, 255);  //Ein Farbton der komplett weiss ist wird erstellt. 
				//Hier werden nun alle 4 Pixel miteinander verglichen ob sie die gleiche Farbe haben.
				if (c.equals(c1) && c1.equals(c2) && c2.equals(c3)) {
					//Sollte alle vier Pixel die gleiche Farbe haben werden sie weiss gef�rbt.
					bimg.setRGB(x, y, white.getRGB());
					bimg.setRGB(x + 1, y, white.getRGB());
					bimg.setRGB(x, y + 1, white.getRGB());
					bimg.setRGB(x + 1, y + 1, white.getRGB());
				}

			}
			
		}
		
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(img, 1, 1, null);
	}
}

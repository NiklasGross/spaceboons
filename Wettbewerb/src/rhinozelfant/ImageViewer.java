package rhinozelfant;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JFileChooser;


public class ImageViewer extends JFrame {

	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel pnlHintergrund;
	private JFileChooser jfco = new JFileChooser();

	
	 // Start des Programms.
	 
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ImageViewer frame = new ImageViewer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public ImageViewer() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setBounds(100, 100, 581, 1029);
		contentPane = new JPanel();	//Das Panel worauf die Ausgaben kommen wird erstellt.
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0)); //gr��e und Layout des Frames einrichten

		JPanel pnlButtons = new JPanel();				//ein Pannel erstellen f�r die Kn�pfe
		contentPane.add(pnlButtons, BorderLayout.SOUTH); 

		JButton btnOeffnen = new JButton("Bild \u00F6ffnen"); //Knopf zum �ffnen erstellen
		btnOeffnen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((ImageJPanel) pnlHintergrund).setImg(jfcoOpenFilename());
				contentPane.repaint();
			}

		});
		pnlButtons.add(btnOeffnen);   //zum Kn�pfe-Panel hinzuf�gen

		JButton finden = new JButton("Rhinozelfant finden"); //Knopf zum anwenden der Suche von Rhinozelfantschuppen
		finden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((ImageJPanel) pnlHintergrund).rhinoFinden();
				contentPane.repaint(); 
			}
		});
		pnlButtons.add(finden); //zum Kn�pfe-Panel hinzuf�gen

		pnlHintergrund = new ImageJPanel(); //Hier wird das Panel wo das Bild drauf ausgegeben wird erstellt.

		contentPane.add(pnlHintergrund, BorderLayout.CENTER); //

	}

	public String jfcoOpenFilename() {  //ausw�hlen einer Biilddatei
		jfco.setDialogTitle("�ffne Datei");
		if (jfco.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			return jfco.getSelectedFile().getPath();
		} else {
			return null;
		}
	}

}

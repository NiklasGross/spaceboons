package rataion;

import java.io.*;

public class Start {
	public static void main(String[] args) throws IOException {
		System.out.println("Bitte legen sie die Datei im selben Verzeichnis ab unter dem Namen 'Rahmen.txt' ab");
		String[][] rahmen = read();
		KennischeAusgabe.ausgeben(rahmen);
		KennischeAusgabe.ausgeben(Rotator.auf_die_rechte_Seite_legen(Rotator.auf_die_linke_Seite_legen(Rotator.auf_die_linke_Seite_legen(rahmen))));
		Loeser.loesen(rahmen);
	}

	public static String[][] read() throws IOException {
		FileReader fr = new FileReader("D:/Dateien/Schule/Rahmen.txt");
		BufferedReader br = new BufferedReader(fr);
		String z1 = br.readLine();
		int arrayhb = Integer.parseInt(z1);
		String[][] rahmen = new String[arrayhb][arrayhb];
		for (int y = 0; y < arrayhb; y++) {
			String zx = br.readLine();
			for (int x = 0; x < arrayhb; x++) {
				rahmen[y][x] = "" + zx.charAt(x);
			}
		}
		br.close();
		return rahmen;
	}
}

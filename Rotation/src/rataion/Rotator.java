package rataion;

public class Rotator {
	public static String[][] auf_die_rechte_Seite_legen(String[][] rahmen) {
		String[][] rahmenv2 = new String[rahmen.length][rahmen.length];
		for (int y = 0; y < rahmenv2.length; y++) {
			for (int x = 0; x < rahmenv2.length; x++) {
				rahmenv2[y][x] = rahmen[rahmenv2.length - 1 - x][y];
			}
		}
		KennischeAusgabe.ausgeben(rahmenv2);
		return gravitation(rahmenv2);
	}

	public static String[][] auf_die_linke_Seite_legen(String[][] rahmen) {
		String[][] rahmenv2 = new String[rahmen.length][rahmen.length];
		for (int y = 0; y < rahmenv2.length; y++) {
			for (int x = 0; x < rahmenv2.length; x++) {
				rahmenv2[y][x] = rahmen[x][rahmenv2.length - 1 - y];
			}
		}
		return gravitation(rahmenv2);
	}

	public static String[][] gravitation(String[][] rahmen) {
		for (int y = rahmen.length - 2; y > 0; y--) {
			for (int x = 1; x < rahmen.length - 1; x++) {
				// checken ob das feld nicht leer ist
				if (false == rahmen[y][x].equals(" ")) {
					// checken ob die selbe zahl links oder rechts ist
					if (rahmen[y][x].equals(rahmen[y][x - 1]) || rahmen[y][x].equals(rahmen[y][x + 1])) {
						int l, r, d;

						// wie weit links
						for (l = 1; rahmen[y][x].equals(rahmen[y][x - l]); l++) {
						}
						l--;

						// wie weit rechts
						for (r = 0; rahmen[y][x].equals(rahmen[y][x + r]); r++) {
						}
						r--;

						// unter dem objekt
						loop: for (d = 1; d + y < rahmen.length; d++) {
							if (false == rahmen[y + d][x].equals(" ")) {
								d--;
								break loop;
							}
							for (int lx = 1; lx <= l; lx++) {
								if (false == rahmen[y + d][x - lx].equals(" ")) {
									d--;
									break loop;
								}
							}
							for (int rx = 1; rx <= r; rx++) {
								if (false == rahmen[y + d][x + rx].equals(" ")) {
									d--;
									break loop;
								}
							}

						}
						// herunterfallen
						if (d > 0) {
							for (int lx = l; lx >= 0; lx--) {
								String help = rahmen[y][x - lx];
								rahmen[y + d][x - lx] = help;
								rahmen[y][x - lx] = " ";
							}
							for (int rx = r; rx > 0; rx--) {
								String help = rahmen[y][x + rx];
								rahmen[y + d][x + rx] = help;
								rahmen[y][x + rx] = " ";
							}
						}

					} else if (rahmen[y][x].equals(rahmen[y - 1][x]) || rahmen[y][x].equals(rahmen[y + 1][x])) {
						int o, u, d;

						// wie weit oben
						for (o = 1; rahmen[y - o][x].equals(rahmen[y][x]); o++) {
						}
						o--;

						// wie weit unten
						for (u = 1; rahmen[y + u][x].equals(rahmen[y][x]); u++) {
						}
						u--;

						// unter dem objekt
						for (d = 1; d + y + u < rahmen.length; d++) {
							if (false == rahmen[y + d + u][x].equals(" ")) {
								d--;

								break;
							}
						}

						// herunterfallen
						if (d > 0) {
							if (u > 0) {
								for (int ux = u; ux > 1; ux--) {
									String help = rahmen[y + ux][x];
									rahmen[y + ux + d][x] = help;
									rahmen[y + ux][x] = " ";
								}
							}
							if (o > 0) {
								for (int ox = 0; ox <= o; ox++) {
									String help = rahmen[y - ox][x];
									rahmen[y - ox + d][x] = help;
									rahmen[y - ox][x] = " ";
								}
							}

						}

					}
				}
			}
		}
		return rahmen;
	}
}
